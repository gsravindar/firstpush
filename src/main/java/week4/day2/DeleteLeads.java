package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class DeleteLeads extends ProjectMethods {
	//@Test(invocationCount=2, timeOut = 3000,enabled = false)
	@Test(groups= {"regression"})
	
	public void DlLeads()
	{
		
		WebElement eleCrm = locateElement("text", "CRM/SFA");
		click(eleCrm);
		WebElement eleCreate = locateElement("text", "Create Lead");
		click(eleCreate);
		WebElement compNme = locateElement("createLeadForm_companyName");
		type(compNme, "IBM");
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, "Ravi");
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, "sekar");
		verifyExactText(lastName, "sekar");
		WebElement dropd = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(dropd, 2);
		WebElement createButton = locateElement("class", "smallSubmit");
		click(createButton);

	}
}
