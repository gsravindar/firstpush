package week4.day2;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class EditLeads extends ProjectMethods {
	@Test(groups= {"sanity"})
	public void EdLeads()
	{	
		//driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement eleCrm = locateElement("text", "CRM/SFA");
		click(eleCrm);
		WebElement eleCreate = locateElement("text", "Create Lead");
		click(eleCreate);
		WebElement compNme = locateElement("createLeadForm_companyName");
		type(compNme, "IBM");
		WebElement firstName = locateElement("createLeadForm_firstName");
		type(firstName, "Ravi");
		WebElement lastName = locateElement("createLeadForm_lastName");
		type(lastName, "sekar");
		verifyExactText(lastName, "sekar");
		WebElement dropd = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingIndex(dropd, 2);
		WebElement createButton = locateElement("class", "smallSubmit");
		click(createButton);
		WebElement editButton = locateElement("text", "Edit");
		click(editButton);
		WebElement fName = locateElement("id", "updateLeadForm_firstName");
		type(fName, "karthi");
		

	}
}
