package week4.day1;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class IrctcSnap {

	public static void main(String[] args) throws IOException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\TestLeaf\\eclipse-workspace\\Selenium\\drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/nget/train-search");
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Contact Us").click();
		
		Set<String> allWindows = driver.getWindowHandles();
		
		List<String> ls = new ArrayList<>();
		
		ls.addAll(allWindows);
		System.out.println("current win title "+driver.getTitle());
		driver.switchTo().window(ls.get(1));
		System.out.println("current win title "+driver.getTitle());
		
		
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./Snaps/pic.jpg");
		
		FileUtils.copyFile(source, dest); 
		
		
		
		
		Set<String> allWindows1 = driver.getWindowHandles();
		
		List<String> ls1 = new ArrayList<>();
		
		ls1.addAll(allWindows1);
		
		for (String s : ls1) {
			
			if(!s.equals(driver.getWindowHandle())) {
				driver.switchTo().window(s).close();
			}
			
		}
		
		
		
		
		
		
		
		

	}

}
