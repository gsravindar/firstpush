package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import week4.day2.ProjectMethods;

public class CreateLead extends ProjectMethods {
	
	
	
	
	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	
	
	
	@FindBy(how = How.ID, using = "createLeadForm_companyName") WebElement eleCName;
	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement eleFName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName") WebElement eleLName;
	@FindBy(how = How.ID, using = "createLeadForm_dataSourceId") WebElement eleCrLeadButton;
	
	public CreateLead enterCompName(String cName) {
		type(eleCName, cName);
		return this;
		}
		
		public CreateLead enterFName(String fName) {
			type(eleFName, fName);
			return this;
			
		}
		
		public CreateLead enterLName(String lName) {
			type(eleFName, lName);
			return this;
			
		}
		
		public ViewLead clickCreateLead() {
			click(eleCrLeadButton);
			return new ViewLead();
			
		}
		
		
		
	}

