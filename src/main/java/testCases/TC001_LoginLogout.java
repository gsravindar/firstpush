package testCases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import week4.day2.ProjectMethods;

public class TC001_LoginLogout extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		
	/*	testCaseName = "Login Logout";
		testDescription = "Login into Leaftap";
		category = "smoke";
		*/
		
		dataSheetName = "TC001";
		
		
	}
	
	
	@Test(dataProvider = "qa")
	public void loginLogout(String uName, String password) {
		
		/*LoginPage lp = new LoginPage();
		lp.enterUserName(uName);*/
		new LoginPage().enterUserName(uName).enterPassword(password).clickLogin();
		
	}
	
	
	
	

}
