package testCases;

import org.testng.annotations.Test;

import pages.LoginPage;
import week4.day2.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	
	
	public void setData() {
		dataSheetName = "TC002";
		
	}
	
	
	@Test
	public void createLead(String uName, String password, String compName, String fname, String lname) {
		new LoginPage().enterUserName(uName).enterPassword(password).clickLogin().clickEleCRMLink().clickLeads().clickCrLead().enterCompName(compName).enterFName(fname).enterLName(lname).clickCreateLead();
	
	}
	
	

}
